<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {
        $title = 'Mike\'s Showcase';
        //return view('pages.index', compact('title'));
        return view('pages.index')-> with('title', $title);
    }

    public function about() {
        $title = 'About Me';
        return view('pages.about')-> with('title', $title);
    }

    public function services() {
        $data = array(
            'title' => 'Services',
            'services' => ['Web Development', 'Database Engineering', 'Security Analysis']
        );
        return view('pages.services')-> with($data);
    }

    public function contact(){
        $title = 'Contact Us Now!';
        return view('pages.contact')-> with('title', $title);
    }
}
